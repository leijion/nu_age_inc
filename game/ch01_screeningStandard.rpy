﻿## The chapter 01 - standard patient Screening.
label ch01_screeningStandard:
    
    $standardScans += 1

    if (comprehensiveScans == 1): 
        $standardScans += 1 

    show nurse normal
    n "Here are the results from the screening. Who is the best candidate?"

    if (standardScans == 1):
        menu:

            "... Sex: Female; Age: 24yrs; Status: Tumours;":
                jump ch01_selection

            "... Sex: Female; Age: 19yrs; Status: Haemophilia;":
                jump ch01_selection

            "... Sex: Female; Age: 33yrs; Status: Healthy;":
                jump ch01_selection

            "... Sex: Female; Age: 36yrs; Status: Healthy;":
                jump ch01_selection


    elif (standardScans == 2):
        menu:

            "... Sex: Male; Age: 26yrs; Status: Healthy;":
                jump ch01_selection

            "... Sex: Female; Age: 17yrs; Status: Haemophilia;":
                jump ch01_selection

            "... Sex: Male; Age: 33yrs; Status: Healthy;":
                jump ch01_selection

            "... Sex: Male; Age: 48yrs; Status: Tumour;":
                jump ch01_selection



    else:
        menu:

            "... Sex: Male; Age: 24yrs; Status: Tumours;":
                jump ch01_selection

            "... Sex: Male; Age: 19yrs; Status: Haemophilia;":
                jump ch01_selection

            "... Sex: Male; Age: 21yrs; Status: Healthy;":
                jump ch01_selection

            "... Sex: Male; Age: 23yrs; Status: Haemophilia & Tumour;":
                $ch01Sabotage = True
                $quality = -30;
                jump ch01_selection