﻿## The chapter 01 - scene Board Intervention.
label ch01_boardIntervention:
    
    $ch01Intervention = True
    show sarah normal

    sh "Sorry Doctor, but the board is not happy with your progress."

    me "Why, what is the problem?"
    
    sh "We cannot keep scanning patients like this, it is costing too much time and money."
    sh "The board has asked that we speed this up."
    
    me "Does the board realise we could end up with unsuitable candidates?"
    
    sh "Yes, they are fully aware. You will not be held accountable."
    sh "You should finish screening your patients, but you must make it brief."
    
    hide sarah normal

    jump ch01_screeningBrief