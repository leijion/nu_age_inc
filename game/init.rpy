﻿# Background Images.
image bg nuAgeInc = "bg/nuAgeInc.png"
image bg hallway = "bg/hallway.png"
image bg hallwayBusy = "bg/hallwayBusy.png"
image bg screeningRoom = "bg/screeningRoom.png"
image bg patientTrialRoom = "bg/labratory.png"
image bg boardroom = "bg/boardroom.png"
image bg blackScreen = "bg/blackScreen.png"
image bg positive = "bg/positive.png"
image bg negative = "bg/negative.png"
image bg neutral = "bg/neutral.png"
image bg after = "bg/nuAgeInc-after.png"
image bg ch01 = "bg/ch01.png"
image bg ch02 = "bg/ch02.png"
image bg ch03 = "bg/ch03.png"

# Main Character Sprites.
image young normal = "character/dr_young.png"
image steele normal = "character/dr_steele.png"
image sarah normal = "character/sarah-holmes.png"
image powers normal = "character/max-powers.png"

# Minor Character Sprites.
image mary normal = "character/mary-mcmahon.png"
image nurse normal = "character/nurse.png"
image patientX healthy = "character/patient-x-healthy.png"
image patientX zombie = "character/patient-x-zombie.png"

# Character Objects.
define y = Character('Dr Young', color="#70AD47")
define s = Character('Dr Victor Steele', color="#FF0000")
define sh = Character('Dr Sarah Holmes', color="#FFC000")
define n = Character('Nu Age Nurse', color="#C0C0C0")
define c = Character('Max Powers', color="#800000")
define m = Character('Mary Mcmahon', color="#92D050")
define x = Character('Patient X', color="#663300")
define me = Character('Me', color="#c8c8ff")

# Audio Objects.
define audio.ch01 = "sound/intro-music.mp3"
define audio.ch02 = "sound/Theyre-Here_Looping.mp3"
define audio.ch03 = "sound/Dizzybot.mp3"
define audio.positive = "sound/Urban-Jungle-2061.mp3"
define audio.neutral = "sound/Dark-Techno-City.mp3"
define audio.negative = "sound/Security-Breach.mp3"

## The game starts here.
label start:

    $time = 0
    $cost =  0
    $quality = 0
    $trust = 0
    $scans = 0
    $briefScans = 0
    $standardScans = 0
    $comprehensiveScans = 0
    $ch01Intervention = False
    $ch01Sabotage = False
    $ch02Intervention = False
    $finalScore = 0

    jump intro

    return