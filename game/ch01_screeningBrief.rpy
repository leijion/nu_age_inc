﻿## The chapter 01 - Brief patient Screening.
label ch01_screeningBrief:
    
    $quality -= 10
    
    $briefScans += 1

    show nurse normal
    n "Here are the brief screening results. Who is the best candidate?"

    if (briefScans == 1):
        menu:

            "... Sex: Male; Age: 24yrs;":
                jump ch01_selection

            "... Sex: Male; Age: 19yrs;":
                jump ch01_selection

            "... Sex: Male; Age: 21yrs;":
                jump ch01_selection

            "... Sex: Male; Age: 23yrs;":
                jump ch01_selection

    elif (briefScans == 2):
        menu:

            "... Sex: Femal; Age: 31yrs;":
                jump ch01_selection

            "... Sex: Female; Age: 19yrs;":
                jump ch01_selection

            "... Sex: Female; Age: 33yrs;":
                jump ch01_selection

            "... Sex: Female; Age: 36yrs;":
                jump ch01_selection

    else:
        menu:

            "... Sex: Male; Age: 26yrs;":
                jump ch01_selection

            "... Sex: Female; Age: 17yrs;":
                jump ch01_selection

            "... Sex: Female; Age: 33yrs;":
                jump ch01_selection

            "... Sex: Male; Age: 48yrs;":
                jump ch01_selection