﻿## The chapter 02 - scene 01.
label ch02_s01:

    play music ch02
    $renpy.music.set_volume(0.5, channel='music')
    scene bg ch02
    with fade
    $renpy.pause(1.0)
    scene bg patientTrialRoom
    with fade
    
    show steele normal at left

    s "Well the Preliminary results are in, and it looks like the trials will be a complete success."

    hide steele normal
    show young normal at right

    y "The healthy patients are retaining their youth and scarring is almost non existent."

    me "That’s fantastic!"

    y "Yes, and the tumour patients are showing remarkable improvements."
    y "In some cases the serum appears to be turning back the clock."
    y "These patients appear younger than when they arrived."
    y "Their tumours are in full remission."

    me "Their tumours are in full remission???"
    me "That is a tremendous result!"

    y "Yes, The serum is re-setting cells back to their natural state."

    hide young normal

    show steele normal at left

    s "So testing your ill candidates may turn out to be our greatest success?"

    hide steele normal

    show young normal at right

    y "Well, yes. Although I suspected tumour patients would experience accelerated regeneration."
    y "The haemophiliac patients were my greatest concern."

    hide young normal

    me "Most of the haemophilia patients have only shown mild positive affects to the serum."
    me "Aside from patient X they have all become highly susceptible to viral infections."
    me "They just keep getting sick!"

    show steele normal at left

    s "Interesting."
    s "Where did these viruses come from?"
    s "The trials were conducted in a sterile environment?"
   
    hide steele normal
    
    me "I don’t know but at this point does it even matter?"
    me "The serum makes the viral infections less severe."
    me "To me this is just another positive result."

    show young normal at right

    y "I hope the serum is not responsible for their illness, Because this result may align with my original suspicions."

    me "And what is that?"

    y "I suspected when the serum is in the presence of a body without sufficient coagulants."
    y "It would create a perfect environment for the mutation of viruses."
    
    me "You suspected the patients could become biological petri dishes?"
    
    y "Yes, and if it’s true, this is a serious problem for us."
    
    y "I think it warrants further investigation."

    hide young normal

    show steele normal at left

    s "Why? The serum limits the effects of viruses anyway."
    s "The positives still outweigh the negatives."

    menu:

        "... Offer to investigate virus theory?":
            hide steele normal
            $time += 5
            me "Okay Dr Young, I will investigate this virus theory further."

        "... Agree, Dr Young should investigate his theory?":
            hide steele normal
            show young normal at right
            y "Regardless I will have to investigate this virus theory."
            hide young normal


        "... Persuade, Dr Steele to investigate the virus theory?":
            $quality -= 10
            s "Yes, I will look into the matter."
            hide steele normal


    jump ch02_s02