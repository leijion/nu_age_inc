## The chapter 03 - scene 01.
label ch03_s01:

    play music ch03
    $renpy.music.set_volume(0.9, channel='music')
    scene bg ch03
    with fade
    $renpy.pause(1.0)
    scene bg boardroom
    with fade
    
    "{i}It's decision time."
    
    "{i}Should we complete our research now by showcasing the serums regenerative powers.{/i}" 
    "{i}We would all receive a substantial bonus though at the risk of viral outbreaks.{/i}"

    "{i}Perhaps we should put ourselves behind schedule by trying to understand Patient X first.{/i}"
    "{i}We would have to forgo our the substantial bonuses at the risk that patient X is an isolated anomaly.{/i}"
    
    "{i}What should I do?{/i}" 

    show young normal at right
    
    y "I hope this goes well."

    hide young normal
    $renpy.pause(0.5)
    show steele normal at left

    s "Let's just present all the facts."

    $renpy.pause(0.5)
    
    hide steele normal
    
    $renpy.pause(1.0)
    show powers normal

    c "I am glad to meet the finest minds in my company."

    if (time >= 10 or quality <= -10):

        c "However, despite being the best in this company."
        c "You'd have me believe your research is yet to yield results."
        c "And it has already exceeded its initial budget."
        c "How can we continue like this Dr Young?"
        
        hide powers normal
        show young normal at right

        y "With all due respect, Mr Powers, we need more time to ensure that the serum is safe."
        
        hide young normal

        show powers normal
        c "Dr Steele disagrees."

        hide powers normal
        show steele normal at left

        s "Yes, Mr Powers."

        hide steele normal
        show young normal at right

        y "Frankly, I am not surprised Dr Steele disagrees."
       
        hide steele normal

        show powers normal
        c "I have received a report that suggest your trials are close to completion and like to be a complete success."
               
        c "Yet unfortunately for you Dr Young, it also suggests you are holding back the trial to pursue a personal endeavour."

        c "Tell me Dr Young, are you more concerned about a single patient than the whole trial?"

        hide powers normal

        show young normal at right
        
        y "Well... Uh... yes... but... uh... we need to account for anomalies."

        hide young normal
        show steele normal at left

        s "It disappoints me greatly to put you in a position like this but we all have to make a choice."
        
        s "Where do you stand?"

    elif (time < 10 and cost >=5):
        
        c "And to what do I owe this pleasure?"

        hide powers normal

        show young normal at right
        
        y "Mr Powers, We have discovered an issue with our research that requires the trials to be immediately halted."

        hide young normal

        show steele normal at left
        
        s "Do you understand what you are saying Dr Young?"

        s "We cannot halt the trials, they are almost complete!"

        hide steele normal

        show powers normal

        c "Dr Steele is right."

        c "We have invested a great deal of time and money into your research Dr Young."

        c "You will need strong evidence to support your claims."

        hide powers normal

        show young normal at right

        y "You have spent a great deal of time on this project."

        y "Why don’t you voice your own opinions?"

    else:
        
        c "And to what do I owe this pleasure to?"

        hide powers normal
        show steele normal at left
        
        s "Mr Powers, there are some contradiction in our results so we need you to decide the future direction of the trials."

        hide steele normal

        show young normal at right
        
        y "Mr Powers, we need to investigate the viruses from Patient X to prevent further spreading and to try and find a cure."
       
        y "We also need to establish the triggers for the viral mutations in haemophiliacs before we can release the serum to the public."

        hide young normal

        show steele normal at left        

        s "There is no need all drugs have side affects and the benefits of the serum vastly outweigh its negatives."
        
        hide steele normal

        show powers normal
         
        c "And what is your opinion doctor?"

    menu:

        "... Work in your own time to investigate.":
            $quality += 15
            jump ch03_s02A

        "... Persuade Mr Powers for more time":
            $time += 5
            $cost += 5
            $quality += 20
            jump ch03_s02B

        "... Agree with Dr Steele":
            $quality -= 10
            jump ch03_s02C