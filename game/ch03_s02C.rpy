## The chapter 03 - scene 02 C.
label ch03_s02C:

    hide steele normal
    hide young normal
    hide powers normal

    with fade
    
    me "I have to agree with Dr Steele."
    me "The research has been delayed long enough we need to proceed on schedule."

    show powers normal

    c "And that is your expert opinion?"

    hide powers normal

    show young normal at right

    y "This is ridiculous. All the evidence points to the contrary."

    hide young normal    
    show steele normal at left

    s "Dr Young, your relentlessness for perfection is admirable. However, it is costing us unnecessary time and money. "

    hide steele normal
    show powers normal

    c "I do apologise Dr Young, I know of your commitment to the serum and the company. But I'm in full agreement with your colleagues."
 
    c "The trials must continue."

    hide powers normal

    show young normal at right

    y "Mr Powers I know this is your company and I respect your decision. However, I urge you to go over the results thus far."
    y "Please, surely you understand the risks if we do not investigate further. Just look at Patient X."


    hide powers normal    
    show powers normal

    c "Who is patient X?"

    hide powers normal
    show steele normal at left

    s "Patient X is of no concern! and despite your circumstances Dr Young you could maintain your grace."

    hide steele normal

    show powers normal

    c "Dr Young you may be excused."

    c "From this point onwards Dr Steele you will head the serum research."

    menu:
 
        "... Agree, patient X is an anomaly":
            $quality -= 5
            jump ending

        "...":
            jump ending

        "... Tell the truth about Patient X":
            $quality += 25
            $trust += 10
            jump ending