﻿## The chapter 02 - Closing Scenes.
label ch02_ending:

    scene bg blackScreen
    with fade
    scene bg screeningRoom
    with fade

    if (quality <= -10):

        show steele normal at left
        
        s "I have investigated the virus theory of patient X and I can verify that there is no risk."

        hide steele normal

        show young normal at right
        
        y "Nonsense! There was more than enough circumstantial evidence to warrant further investigation."

        y "I will examine him again myself!"

        hide young normal

        show mary normal

        m "Excuse me doctors but Mr Powers has requested that you all attend him in the boardroom."
        hide mary normal
    elif (cost == 5):

        
        "{i}I have examined patient X and this new virus and I think we may have a serious problem.{/i}"

        me "It appears that most of the new viruses that have spread to the other candidates originated from Patient X."
        me "Not only is Patient X’s body mutating viruses his accelerated healing properties are forcing the mutations to accelerate."

        show young normal at right

        y "Are you positive? We must get this to the board and ensure that the drug is not yet released."
        hide young normal
    else:
        show young normal at right

        y "It is as I feared we have a serious problem."
        y "It appears that most of the new viruses that have spread to the other candidates originated from Patient X."
        y "Not only is Patient X’s body mutating viruses his accelerated healing properties are forcing the mutations to accelerate."
        
        hide young normal
        show steele normal at left 

        s "Are you positive?"
        s "This completely contradicts my own research."
        s "Let us take what we have to the board and let them decide."

        hide steele normal

    if (quality == 0):
        $finalScore += 10
    elif(time == cost and quality == -10):
        $finalScore += 5
    else:
        $finalScore += 0

    jump ch03_s01