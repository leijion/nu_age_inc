﻿## The chapter 01 - selection choice continues patient selection loop.
label ch01_selection:

    if (scans < 3):
   
        show nurse normal

        if (scans == 1):
            n "We are ready to screen the second group."
            n "How should they be screened?"
        else:
            n "The final group is prepared."
            n "How should they be screened?"

        jump ch01_selectionChoice

    show nurse normal
    n "That is everyone doctor."
    n "Shall I escort the chosen candidates to their trial rooms?"

    me "Yes, thank you Nurse."

    if (quality <= -20): # Negative
        $finalScore += 0
    elif((cost == 10 and quality >= -10) or (cost == 5 and quality >= 0)): # Positive
        $finalScore += 10
    else: # Neutral
        $finalScore += 5 

    $time = 0
    $cost =  0
    $quality = 0

    jump ch02_s01
