﻿## The chapter 02 - scene 02.
label ch02_s02:

    scene bg blackScreen
    with fade
    scene bg screeningRoom
    with fade
    
    me "It appears Dr Young is right. Not only are all the haemophiliacs infected with different viruses but they are all carrying a new strain of virus never before seen."

    show young normal at right

    y "What about the haemophiliac known as patient X?"

    hide young normal
    show steele normal at left

    s "He is without doubt our star candidate. He is one of the haemophiliacs but has shown regenerative abilities better than all other candidates."

    hide steele normal
    show young normal at right
    
    y "And..? What sets him apart from the rest of his group?"
    
    y "Why is he showing a greater acceptance to the serum than the other haemophiliacs? Why isn’t he sick?"

    hide young normal

    "{i}It looks like Dr Steele has no answer for this.{/i}"

    show sarah normal

    sh "Dr Young!"

    sh "I have performed a comprehensive screening of Patient X as per your request."
    sh "It appears you were right!"
    sh "Patient X is not only a haemophiliac, but he is also in remission!"
    sh "He had tumours."
    
    hide sarah normal

    show young normal at right

    y "Thank you Sarah"

    show sarah normal

    sh "It's no problem doctor, I thought you should know."

    hide sarah normal    
    show young normal at right

    y "Doctors, I specifically requested that you screen out these candidates."

    if (ch01Sabotage == True):
        $trust = -20;
        me "Hmm. . . it seemed appropriate to put him through."
    elif (ch01Intervention == True):
        me "Sorry doctor. The board would not let me screen patients properly, they said it was costing too much time and money."
    elif (finalScore == 0):
        me "Apologies Dr Young, I must have missed something during the screening."
    else:
        me "Dr Steele?"

        show steele normal at left

        s "He was pushed through by the board."
        hide steele normal
    
    show young normal at right
    y "So we have Patient X, who is showing accelerated regenerative properties and not showing any signs of illness."
    y "Let’s hope he is not also a carrier of the new virus that's spread to everyone else in his ward."
    y "This patient X needs to be examined closely."
    hide young normal
    menu:

        "... Offer to examine patient X?":
            $time += 5
            $cost += 5

        "... Agree, Dr Young should look into it?":
            $time += 0

        "... Persuade, Dr Steele to examine patient X?":
            $quality -= 10;

    if (time == 10):
        jump ch02_boardIntervention
    else:
        jump ch02_ending