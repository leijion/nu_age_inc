## Neutral ending.
label end_neutral:

    play music neutral
    $renpy.music.set_volume(0.8, channel='music')
    scene bg screeningRoom

    "{i}Dr Victor Steele made a much more appealing proposal to the board.{/i}"
    "{i}Due to the length of time and overspend Dr Steele was promoted to head scientist.{/i}"
  
    show steele normal at left

    s "Looks like the board agreed with me."

    me "Where is Dr Young?"

    s "He is gone this is my project now."

    hide steele normal

    "{i}. . .{/i}"
    
    me "Dr Young was fired, that's a shame."

    me "It's been a long day, it's time for me to go home anyways."

    scene bg nuAgeInc

    "{i}That looks like Dr Young.{/i}"

    show young normal at right

    me "Dr Young, it is a shame about what happened."
    
    y "Well, I am actually the one who decided to leave."
    y "I have been offered twice the funding at a different company where I can continue my research."

    if (trust >= 10):

        me "That's great Dr Young."

        "{i}Dr Young gives me his new card.{/i}"
        
        y "Let me know if you're interested."
        y "Dr steele believes he won my anti ageing serum along with my position."
        y "But he's only going to find an empty lab."

        hide young normal

    else:
        me "But we now have your Anti ageing serum."

        y "Nope, It's still MY Anti ageing serum."
        y "Farewell!"

        hide young normal

        "{i}!!!?{/i}"

        me "I guess we are no longer friends."

        show steele normal at left

        s "Have you seen Dr Young."

        me "Yes, he just left."

        s "He's taken everything!"
        s "His work, the serum, everything!"

        "{i}. . .{/i}"

        me "Then I guess we're both out of a job too."

    scene bg neutral
    $renpy.pause(5.0)