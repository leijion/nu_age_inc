﻿## The chapter 02 - scene Board Intervention.
label ch02_boardIntervention:
    
    $ch02Intervention = True
    $quality -= 10

    show sarah normal

    sh "Sorry Doctor but the board is not happy with the excess overtime."

    me "Why, what is the problem?"

    sh "I cannot allow you to study patient X as you have already spent your lab time investigating the virus theory."
    sh "the board has asked that we equalise the workload to reduce overtime."

    me "Does the board realise how important this reasearch is?"

    sh "Yes, but unfortunately you have spent your lab quota, I am going to have Dr Steele investigate patient X."

    hide sarah normal

    jump ch02_ending