﻿## The game Intro starts here.
label intro:
    
    play music ch01
    $renpy.music.set_volume(0.8, channel='music')

    scene bg nuAgeInc
    with fade
    "{i}Nu Age Inc...{/i}"
    "{i}This is where I work.{/i}"
  
    scene bg hallway
    with fade
    "{i}A multi-national pharmaceutical corporation about to test the anti-ageing serum, a cure for cellular senescence.{/i}"
    "{i}The lead researcher - Dr Young believes it will put an end to biological ageing.{/i}"
    "{i}And bring about a world of perpetual youth.{/i}"
   
    scene bg hallwayBusy
    with fade
    "{i}Today's the day.{/i}"
    "{i}The first clinical trials will make or break this company.{/i}"
    "{i}Dr Young is waiting for me.{/i}"
    "{i}It's time to get started.{/i}"
    
    menu:

        "... Find Dr Young.":
            jump ch01_s01

        "... Look around first":
            $time += 5
            jump delayIntro