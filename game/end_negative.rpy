## Negative ending.
label end_negative:

    play music negative
    $renpy.music.set_volume(0.7, channel='music')
    scene bg screeningRoom

    "{i}The trial has been a success.{/i}"
    "{i}. . .{/i}"
    "{i}. . .{/i}"
    "{i}. . .{/i}"
    "{i}Or has it?{/i}"

    show mary normal

    m "Congratulations with the trial."
    m "Please meet the patient who started your success."


    hide mary normal

    show patientX healthy

    x "Hello doctor!"

    show patientX healthy

    x "Thank youuuuuuuuuuuuuuuu."
    
    show patientX zombie at left

    m "Aaaaaaaaaaah!"
   
    scene bg after

    "{i}. . .{/i}"
    "{i}There was a reason for trials to exclude certain patients.{/i}"
    "{i}. . .{/i}"
    "{i}Their genetic makeup together with specific illnesses trigger undesired mutations.{/i}"
    "{i}The evidence was pointing to this early on.{/i}"
    "{i}But we chose to ignore it.{/i}"
    "{i}It changed the patients.{/i}"
    "{i}. . .{/i}"
    "{i}Into Zombies.{/i}"
    "{i}. . .{/i}"
    "{i}Then it spread.{/i}"

    scene bg negative
    
    $renpy.pause(5.0)