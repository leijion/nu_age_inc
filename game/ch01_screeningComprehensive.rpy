﻿## The chapter 01 - Comprehensive Screening.
label ch01_screeningComprehensive:
    
    $time += 5
    $cost += 5

    $comprehensiveScans += 1

    if (standardScans == 2 and comprehensiveScans < 2): 
        $comprehensiveScans += 1

    show nurse normal
    n "Here are the results from the comprehensive screening. Who is the best candidate?"

    if (comprehensiveScans == 1):

        menu:

            "... Sex: Female; Age: 24yrs; bio-age: 29; Status: Tumours;":
                jump ch01_selection

            "... Sex: Female; Age: 19yrs; bio-age: 29; Status: Haemophilia;":
                jump ch01_selection

            "... Sex: Female; Age: 33yrs; bio-age: 31; Status: Healthy;":
                jump ch01_selection

            "... Sex: Female; Age: 36yrs; bio-age: 52; Status: Healthy;":
                jump ch01_selection

    else:
        menu:
            "... Sex: Male; Age: 24yrs; bio-age: 29; Status: Tumours;":
                jump ch01_selection

            "... Sex: Male; Age: 19yrs; bio-age: 28; Status: Haemophilia;":
                jump ch01_selection

            "... Sex: Male; Age: 21yrs; bio-age: 20; Status: Healthy;":
                jump ch01_selection

            "... Sex: Male; Age: 23yrs; bio-age: 48; Status: Haemophilia & Tumour;":
                $ch01Sabotage = True
                $quality = -30;
                jump ch01_selection
