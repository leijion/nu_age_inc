## The chapter 03 - scene 02 A.
label ch03_s02A:

    hide steele normal
    hide young normal
    hide powers normal
    
    with fade
    
    me "I have a method to complete the investigation."

    show steele normal at left

    s "You’re aware there is limited time and resources?"

    hide steele normal

    show powers normal

    c "Unfortunately, I will have to agree with Dr Steele."

    hide powers normal

    me "No additional resources will be used."

    show young normal at right

    y "How will you achieve this?"

    hide powers normal
    show powers normal

    c "Do you intend to complete the research in your own time?"

    hide powers normal
    show steele normal at left

    s "That would be absurd."

    hide steele normal
    show young normal at right

    y "You have not answered our questions."

    y "I will ask you again."

    y "How do you intend to achieve this?"

    me "Yes ... I will answer you!"
    me "I intended to start researching more in my own time."

    y "You haven't got much time as it is, there is never any spare time."
    y "It would be extremely hard to complete."

    hide young normal

    show powers normal

    c "You can do as you will but understand your methods still need to fall in-line with company policy and procedures."

    hide powers normal

    menu:
 
        "... You agree with Dr Young so try to persuade Mr Powers for more time":
            $time += 5
            $cost += 5
            $quality += 5
            $trust += 5
            jump ch03_s02B

        "... Persuade Dr Young to assist in his own time":
            $trust += 5
            jump ending

        "... Backtrack and advise Mr Powers Dr Steele is right":
            $quality += -10
            $trust += -10
            jump ch03_s02C