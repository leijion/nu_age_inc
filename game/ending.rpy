## The calculate the ending scenees.
label ending:

    # Add Score from chapter 03 to final score.
    if (quality == 20):
        $finalScore += 10
    elif(quality < 20 and quality > 0):
        $finalScore += 5
    else:
        $finalScore += 0

    # go to ending based on final score.
    if (finalScore >= 20):
        jump end_positive
    elif (finalScore <= 10):
        jump end_negative
    else:
        jump end_neutral