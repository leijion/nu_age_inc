﻿## The chapter 01 - scene 01.
label ch01_s01:

    scene bg ch01
    with fade
    $renpy.pause(1.0)
    scene bg screeningRoom
    with fade

# Dr Young is right so shown on the right :)    

    show young normal at right

    y "Good morning Doctor."

    hide young normal

    if (time == 0):

        show young normal at right

        y "Glad you could make it on time."
        y "We have some new members of our team."
        y "Let me introduce you."
        y "This is Dr Sarah Holmes, she'll be heading up the pathology lab."

        hide young normal

        show sarah normal        

        sh "Hi there Doctor."

        me "Nice to meet you Dr Holmes."

    else:

        show young normal at right
        
        y "Good, you are finally here."
        y "We are already behind schedule!"

        y "This is Dr Sarah Holmes, she'll be heading up the pathology lab."

        hide young normal

        show sarah normal

        sh "Doctor."

        hide sarah normal

        me "Dr Holmes."

    show young normal at right
    
    y "Dr Holmes, please have nurses run the CAT scan on subject M and subject X."
    y "I want to ensure we don't pass haemophilia subjects who have tumours."
    hide young normal

    hide young normal

    show sarah normal

    sh "Yes Doctor."

    hide sarah normal

    show young normal at right

    y "Well, I’m glad you made it. We have a lot of work to get done and we’ll need your full attention and commitment on this project."
    y "We are about to validate my breakthrough and I need you to start filtering out the subjects to select our trial candidates."
    y "However, you should know that I am concerned about some issues that showed up during the first animal trials."
    y "Firstly, we found the serum had an accelerated affect in the presence of tumours."
    y "And secondly, there were anomalies in animals with low blood counts."
    y "I have a theory about both of these issues, which need to be tested."
    y "So I asked for volunteers with tumours and volunteers suffering with haemophilia."
    y "It will be your job to select the trial candidates. So I ask that you include some of these volunteers into the trial."
    y "However, please ensure candidates do not have both conditions as I suspect they will suffer unnecessarily, it could possibly even put their lives in danger."

    hide young normal

    show steele normal at left

    s "Nonsense! They would not be at risk, and we would only be wasting time."

    hide steele normal

    show young normal at right

    y "I understand your concern Dr Steele, however we need to ensure the safety of our patients."

    y "Doctor, this is your fellow colleague Dr Victor Steele."

    hide young normal

    show steele normal at left

    if (time == 5):

        s "We have already met."

    else:

        s "Nice to met you Doctor."

        me "Hello Dr Steele"

        s "I would have been here earlier but I had an errand to run."

        me "No problem."

    s "Dr Young has accumulated a lot of experience over the years. With the research and data we have collected so far, he could run these trials on healthy people."

    s "Yet he insists on tainting the research with problematic candidates, and not just a few."
    s "He insists we conduct trials on two seperate pools of sick candidates."
    s "Conducting this trial on sick candidates will be a waste of resources."

    s "Dr Young, when you have a board of Directors anxious for results. You need to take action, take the bull by the horns my friend." 
    
    hide steele normal
    show young normal at right

    y "We need to assure that these problems do not pose any real risks!"

    hide young normal
    show steele normal at left

    s "Well if we must risk our trials we should at least reduce the number of problematic candidates."

    hide steele normal
    show young normal at right

    y "We can not obtain conclusive results without a sufficient number of candidates! You know this Dr Steele."

    hide young normal
    show steele normal at left

    s "I know, but we could combining the problematic candidates into a single pool by selecting only those with both illnesses."
  
    hide steele normal
    show young normal at right

    y "Testing and trialling is the core to scientific discovery. And each test must be carried out in isolation."
    y "In addition, I believe those subjects could be at risk."

    hide young normal
    show steele normal at left

    s "I stand by my statement. We only choose sick candidates who are haemophiliacs with tumours."

    hide steele normal
    show young normal at right
    
    y "We have already discussed this matter, we should only trial subjects with low risk."

    hide young normal

    "{i}There is a lot of tension between these doctors. It is clear they both have an agenda of their own.{/i}"
    
    show young normal at right
    y "Very well, I have other matters to look into. Dr Steel, we’ll continue this conversation at another time."
    
    y "For now, I need the both of you to examine patient histories carefully."
    y "The decisions for trial candidates are ultimately yours and I trust you will both make the best choices."
  
    hide young normal


    show steele normal at left

    s "See you soon Dr Young."
    
    s "What a waste of time, don't you agree? Having to read charts and patients history. It seems that we have the success within our grasp. But Dr Young is playing safe and it is just out of our grasp."
    s "You know, if you were to help speed up this process, the board of directors will certainly hear of your contributions."
    s "I can assure you that if we meet the deadline there is a substantial bonus for all of us."

    me "How can you be so sure?"

    s "I have already discuss this with the board, remember the errand I had to run?"
    
    hide steele normal

    "{i}Is Dr Steel asking me to shortcut the research on patients so we can run direct to trials?{/i}"

    show steele normal at left
 
    s "Of course, don't get me wrong, we don't want to rush anything and let down Dr Young."
    s "His traditional methods of research are the very reason the board asked others like you and I, to assist."
    s "Although, if we can't meet the deadline, I am sure we can work on another project later."
    s "Perhaps you and I are not destined to validate this amazing discovery."
    s "Perhaps we are just here so Dr Young can continue his research?"  
    s "Well, I guess I will leave you to it"
 
    hide steele normal
  
    $renpy.pause(2.0)
  
    show nurse normal
 
    n "Doctor we have prepared the first patients for screening."
    n "Please let us know how thorough we need to be."
 
    jump ch01_selectionChoice