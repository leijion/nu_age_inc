﻿## The Delay path of chapter 01.
label delayIntro:

    show steele normal

    s "Hello, My name is Dr Victor Steele."
    s "We are going to be working together during the clinical trials."

    me "Hello Dr Steele"

    s "I am on an errand, you should go on ahead."
    s "Dr Young and Dr Holmes are already waiting for you in the selection room."
    
    hide steele normal
    jump ch01_s01