## The chapter 03 - scene 02 B.
label ch03_s02B:

    hide steele normal
    hide young normal
    hide powers normal   
   
    with fade
    
    me "I have also conduct my own research Mr Powers and my research supports Dr Young."

    show young normal at left

    y "We have discovered a problem and simply need more time."

    hide young normal
    show powers normal

    c "And your results hold more weight than Dr Steele's?"

    hide powers normal
    show steele normal at left

    s "This is nonsense I am a renown scientist in my field."

    hide steele normal

    me "Please just have a look at our research Mr Powers so much has already been spent."
    me "We could solve these problems if we had more time."

    show powers normal 
    c "let me think a moment . . . "

    hide powers normal
    show steele normal at left
    s "!!!?"

    s "I will not stay to see my research debunked by the likes of these two."

    hide steele normal

    show powers normal

    c "{size=+10}MARY!{/size} GET Dr Holmes up HERE."

    hide powers normal
    show mary normal
   
    m "Yes Mr Powers."
    
    hide mary normal    

    $renpy.pause(1.0)

    show powers normal
   
    c "Dr Holmes can we have your expertise please."

    hide powers normal
    show sarah normal

    sh "How may I be of assistance, Mr Powers?"

    hide sarah normal

    show powers normal

    c "Please have a look over these results."

    hide powers normal
    show sarah normal

    sh "Based on the results available, I would be inclined to suggest further investigation Mr Powers."
    sh "These results from patient X could pose a real threat for us."

    hide sarah normal

    show powers normal

    c "Very well doctors."

    c "Given the success of the serum in other areas I will allow you to delay the trials."
    c "Investigate these issues with patient X and find a solution!"
    c "But ... Understand this doctors."
    c "You do not have free rein, I still require results."
    c "Just make sure it's safe."

    $renpy.pause(1.0)


    c "I will have someone replace Dr Steele."
    c "Now get back to work!"

    jump ending