## Positive ending.
label end_positive:

    play music positive
    $renpy.music.set_volume(0.8, channel='music')
    scene bg screeningRoom

    "{i}The trial has been a success.{/i}"

    show mary normal

    m "Congratulations with the trial."
    m "Please meet the patient who started your success."

    hide mary normal
    show patientX healthy

    x "Hello doctor!"
    x "Thank you for giving me a new life, a new beginning."

    me "You are most welcome."

    hide patientX healthy
    show mary normal

    m "Mr Powers would like to see you now."

    hide mary normal

    scene bg boardroom
    show powers normal

    c "Thank you for your work."
    c "On behalf of the company and the board..."
    c "I am happy to offer you this promotion."

    c "The position is head scientist in drug research and development."

    me "!!!?"
    me "Where is Dr Young going?"

    if (trust >= 10):
        hide powers normal
        show young normal at right

        y "Thanks to you, I am now the new Chief Research Officer."
        y "A position which has granted me a seat on the board." 
        y "We look forward to seeing what new products you can develop for the company."

        me "Thank you Dr Young, it has been a pleasure working with you."

        me "And thank you Mr Powers."

    else:
      
        c "Dr Young is now the Chief Research Officer..."
        c "And also sits as our newest member of the board."

        me "Thank you Mr Powers."

        hide powers normal

        "{i}I should have been more helpful towards Dr Young.{/i}"
        "{i}It looks like I'm still going to have to earn his trust.{/i}"

    scene bg positive
    $renpy.pause(5.0)