﻿## The chapter 01 - scene 01.
label ch01_selectionChoice:
    
    $scans += 1

    if (ch01Intervention == False):
        menu:

            "... Comprehensive screening.":
                if (time == 10):
                    hide nurse normal
                    jump ch01_boardIntervention

                else:
                    jump ch01_screeningComprehensive

            "... Standard screening":
                jump ch01_screeningStandard

            "... Brief scan":
                jump ch01_screeningBrief
    else:

        menu:

            "... Brief scan":
                jump ch01_screeningBrief